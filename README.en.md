### ReadMe English [ https://gitee.com/trgtokai/gitee-user-guide/blob/master/README.en.md]( https://gitee.com/trgtokai/gitee-user-guide/blob/master/README.en.md)
### Japanese Japanese [https://gitee.com/trgtokai/gitee-user-guide/blob/master/README.jp.md](https://gitee.com/trgtokai/gitee-user-guide/blob/master/README.jp.md)
-----------------------------------------------------------------------------


### Organization name: [TRG PE]

**The main goal of this organization is to communicate and learn about IOT, AI detection, and industrial automation projects.<br>
If you need to quote the content or code of this organization, please comply with relevant laws and open source agreements. Thank you for your cooperation! ** <br>

### Organization Introduction:
The purpose of creating this organization is only for personal IOT AI detection project communication and learning<br>
Mainly used for code management and sharing of IOT AI detection and other innovative projects<br>
Currently only open to internal personnel of the organization, or designated personnel to send network addresses<br>

### Projects list:
**NO. Project name Project---------------Introduction<br>**
1. scratch-detect-----------------AI detection program used for appearance or other categories of recognition, requires training of special models<br>
2. mini data base-----------------A VBA-based EXCEL data collection program with an independent UI input interface, and data is stored in sheet<br>
3. 3D printer and CNC engraving machine application-------Introduction to some application scenarios of 3D printers and CNC engraving machines<br>
4. stm32 arduino------------------Use microcontrollers or ARM motherboards to realize automatic control of treatment tools (more complex logic judgment scenarios)<br>
5. User guide ReadMe-----------------Introduction to how to use the code provided by this organization and precautions <br>


download the code as a guest or log in to the website.
1. China website: [ https://gitee.com/trgtokai](https://gitee.com/trgtokai )
2. Overseas websites (no project uploaded): [ https://github.com/bigcheng123](https://github.com/bigcheng123 )

### Precautions
1. For information security reasons, please do not post any information about the company or yourself on this platform
gitee /GitHub platform currently used by the organization is not an enterprise version. The code released by this organization is only for personal communication and learning .
3. All codes released by this organization are from the Internet or personal open source, and comply with the laws and regulations of the People's Republic of China .
If there is any infringement risk or any illegal matter, please contact us immediately .
4. Contact email: bigcheng123@163.com

Established: July 2022< br >
Organization Manager : Guan Gong < br >
Member: Mr. Li, the engineer of Guan Gong < br >


### How to download the code?
**Step 1. First register a Gitee account and log in to the website** < br >
(Public projects can be downloaded without logging into Gitee , private projects require logging in and joining the organization )

![ Enter image description](6.JPG)< br >

**Step2. Open the TRG PE organization website** < br >
[TRG PE organization website https://gitee.com/trgtokai](https://gitee.com/trgtokai)

**Step 3. Open the project and click on the upper right corner to download the code** < br >

![ Enter image description](image4.JPG)< br >

**Step 4. Configure the development environment of the local computer**
1. Install pycharm or other IDE tools
2. Install Anaconda Python package integration tool
3. Install dependent libraries → Install package `pip install requirments.txt`


### How to join the organization?
After joining the organization, you can share all projects under the organization directory and participate in code development .
How to join the organization < br >
1. Register an account and apply to join [ TRG PE]. After the administrator approves your application, your gitee account will be added to [ TRG PE ] < br >
2. Send your Gitee account email to the administrator email address bigcheng123@hotmail.com




### Gitee (code hosting website) usage guide

2. Gitee Official blog [blog.gitee.com](https://blog.gitee.com)
3. You can visit [https://gitee.com/explore](https://gitee.com/explore) to learn about the excellent open source projects on Gitee
4. [GVP](https://gitee.com/gvp) The full name is Gitee's Most Valuable Open Source Project, which is an excellent open source project that has been comprehensively evaluated.
5. User manual provided by Gitee [https://gitee.com/help](https://gitee.com/help)

### How to use git tools
1. Explain that Gitee is a code hosting website, which is similar to Github (open source community) abroad . Log in to the website to view/manage projects (projects stored in the cloud)
2. git is a software used to manage and control the updated version of the code. It is installed on the local computer and used (manage projects stored on the local computer → modify the code locally → upload it to the cloud ( gitee ))
3. View the official git tool instructions
[Official git usage guide https://gitee.com/all-about-git](https://gitee.com/all-about-git)


#### References or citation code
1. [ yolov5 https://github.com/ultralytics/yolov5/tree/v6.1](https://github.com/ultralytics/yolov5/tree/v6.1)
2. [ OpenCV ](http://)
3. [ pytorch ](http://)
4. [ python](http://)
5. [pyQT 5]( http://)
6. [Enter link description](http://)
7. [Enter link description](http://)
8. [Enter link description](http://)
9. [Enter link description](http://)
10.[Enter link description](http://)


#### Stunts
1. Use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
6. Gitee Cover Star is a column to showcase the style of Gitee members [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
