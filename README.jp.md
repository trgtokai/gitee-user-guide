### Readme 英語 [ https://gitee.com/trgtokai/gitee-user-guide/blob/master/README.en.md]( https://gitee.com/trgtokai/gitee-user-guide/blob/master/README.en.md)
### 日本語 日本語 [https://gitee.com/trgtokai/gitee-user-guide/blob/master/README.jp.md](https://gitee.com/trgtokai/gitee-user-guide/blob/master/README.jp.md)
……………………………………………………………………………………………………………………………………

### 組織名: [TRG PE]

 **この組織の主な目的は、IoT、AI 検出、産業自動化 プロジェクトについてコミュニケーションし、学ぶことです。<br>
この組織のコンテンツやコードを引用する必要がある場合は、関連する法律およびオープンソース契約に従ってください**<br>

### プロジェクトのリスト:
 **NO. プロジェクト名------概要<br>**
1. スクラッチ検出 ------------------AI 検出プログラムは外観またはその他のカテゴリの認識に使用され、専用モデルのトレーニングが必要です<br>
2. ミニデータベース--独立したUI入力インターフェースとシートにデータが保存されたVBAベースのEXCELデータ収集プログラム<br>
3. 3D プリンターと CNC 彫刻機の応用 -------3D プリンターと CNC 彫刻機のいくつかの応用シナリオを紹介します<br>
4. stm32 arduino-----------------ワンチップマイコンまたはARMマザーボードを使用して、治工具の自動制御（より複雑なロジック判断）を実現します<br>
5. 利用ガイド ReadMe -----------当組織が提供するコードの使い方や注意事項を紹介します<br>



### 組織の紹介:
この組織を設立する目的は、個人のIoT AI検出プロジェクトの交流と学習のみです。 <br>
主にIoT AI検出などの製造革新プロジェクトのコード管理と共有に使用されます<br>
現在、組織の内部担当者のみに公開されている、また指定された担当者がネットワーク アドレスを送信します。 <br>

ゲストとして、または Web サイトにログインした後にコードがダウンロードできます。
1. 中国ウェブサイト: [https://gitee.com/trgtokai](https://gitee.com/trgtokai)
2. 海外のウェブサイト (プロジェクトはアップロードされていません): [https://github.com/bigcheng123](https://github.com/bigcheng123)

### 注意事項
1. 情報セキュリティ上の理由から、会社またはあなた自身に関する個人情報をこのプラットフォームに公開しないでください。
2.組織が現在使用しているgitee /GitHub プラットフォームは個人/組織バージョンで、企業バージョンではありません。この組織がリリースしたコードは個人的なコミュニケーションと学習のみを目的としています。
3. この組織が公開するすべてのコードは、転載または個人のオープンソースから来て、中国の法律と規制に準拠しています。<br>
侵害リスク項目または法的問題がある場合は、すぐにご連絡ください。 <br>
4. 連絡先メールアドレス：bigcheng123@163.com

設立日： 2022年7月<br>
組織マネージャー: Alex Kwan <br>
メンバー:Li-CheY / Zhuo / Lin / Yin<br>


### コードをダウンロードするにはどうすればよいですか?
**ステップ　1. まずGiteeアカウントを登録し、Webページにログインします** <br>
(Giteeに公開プロジェクトについてログインせずにダウンロードできますが、非公開プロジェクトはログインして組織に参加する必要があります) <br>

![输入图片说明](02_Image/image1.JPG)<br>

**ステップ 2. TRG PE 組織の Web サイトを開きます** <br>
[TRG PE組織ウェブサイト https://gitee.com/trgtokai](https://gitee.com/trgtokai)

**ステップ 3. プロジェクトを開き、右上隅をクリックしてコードをダウンロードします** <br>

![输入图片说明](02_Image/image4.JPG)<br>

**ステップ 4. ローカル コンピューターの開発環境を配置します**
1. pycharmまたはその他の IDE ツールをインストールします
2. Anaconda Python パッケージ統合ツールをインストールします
3. 依存ライブラリをインストール → パッケージ`pip install required.txt`をインストール

**ステップ  5.コードを実行します**

IDE （Pycharm / visualstudio）を使って、プロジェクトファイルを開き、デバッグまたはコードを変更します。

### 組織に参加するにはどうすればよいですか?
組織に参加すると、組織ディレクトリ内のすべてのプロジェクトを共有し、コード開発に参加できるようになります。 <br>
組織への参加方法<br>
1. アカウントを登録し、 [TRG PE]への参加申請を行い、管理者の審査が承認されると、 [TRG PE]にgiteeアカウントが追加されます。
2. Giteeアカウントの電子メールを管理者の電子メール bigcheng123@hotmail.com に送信します。




### gitee (コードホスティング Web サイト) ユーザーガイド

1. gitee 公式ブログ[blog.gitee.com](https://blog.gitee.com)
2. Giteeの優れたオープンソース プロジェクトについては、 [https://gitee.com/explore](https://gitee.com/explore) で学ぶことができます。
3. [GVP](https://gitee.com/gvp)全称はGiteeの最も価値のあるオープンソースプロジェクトであり、総合的に評価された優れたオープンソースプロジェクトです。
4. Giteeが提供する公式ユーザーマニュアル[https://gitee.com/help](https://gitee.com/help)

### git ツールの使用方法
1. Gitee が海外のGithub (オープンソース コミュニティ)と同様の機能を備えたコード ホスティング Web サイトであることを説明します。Web サイトにログインしてプロジェクト (クラウドに保存されているプロジェクト) を表示/管理します。

2. git は、コードの更新バージョンを管理および制御するために使用されるソフトウェアであり、ローカル コンピューターにインストールして使用されます (ローカル コンピューターに保存されているプロジェクトを管理 → ローカルでコードを変更 → クラウド ( gitee ) にアップロード)
3. 公式 git ツールの説明資料を参照する
[公式 git 使い方ガイド https://gitee.com/all-about-git](https://gitee.com/all-about-git)


#### 参考文献または引用コード
1. [yolov5](https://github.com/ultralytics/yolov5/tree/v6.1)
2. [OpenCV](https://opencv.org/)
3. [PyTorch](https://pytorch.org/)
4. [Python](http://www.python.org/)
5. [PyQt 5](https://github.com/pyqt)
6. [リンクの説明を入力](http://)
7. [リンクの説明を入力](http://)
8. [リンクの説明を入力](http://)
9. [リンクの説明を入力](http://)
10. [リンクの説明を入力](http://)


#### 特記
1. Readme\_XXX.md を使用して、Readme\_en.md、Readme\_zh.md などのさまざまな言語をサポートします。
6. Gitee Cover Character は、 Giteeメンバーのスタイルを紹介するために使用されるコラムです[https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
