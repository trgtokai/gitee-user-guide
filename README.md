### Readme English [https://gitee.com/trgtokai/gitee-user-guide/blob/master/README.en.md](https://gitee.com/trgtokai/gitee-user-guide/blob/master/README.en.md)
### 日本語 Japanese [https://gitee.com/trgtokai/gitee-user-guide/blob/master/README.jp.md](https://gitee.com/trgtokai/gitee-user-guide/blob/master/README.jp.md)
………………………………………………………………………………………………………………………………………………………………


### 组织名称：【TRG PE]

 **本组织主要目标是 交流，学习IOT、AI检测、工业自动化项目，<br>
如需要引用本组织内容或代码，请遵守相关法例 开源协议，感谢配合！** <br>

### 组织介绍：
创建此组织的目的仅为个人 IOT AI检测项目的 交流学习<br>
主要用于IOT AI检测等制法革新项目的代码管理与共享<br>
目前仅向组织内部人员开放，或指定人员发送网络地址<br>

### Project 项目清单：
 **NO. 项目名称Project---------------简介 Introduction<br>** 
1. scratch-detect-----------------AI检测程序 用于外观或其他类别的识别，需要训练专用模型<br>
2. mini data base-----------------一个基于VBA的 EXCEL数据收集程序，有独立的UI输入界面，数据存储在sheet<br>
3. 3D打印机 及 CNC雕刻机 运用-------介绍 3D打印机 及 CNC雕刻机的一些应用场景<br>
4. stm32 arduino------------------利用单片机或ARM主板 实现治工具的自动化控制（较为复杂的逻辑判定场景）<br>
5. 使用指南 ReadMe-----------------介绍如何使用本组织提供的代码以及注意事项 <br>


你可以以游客或登录网站后 下载代码使用
1. 中国区网址：[https://gitee.com/trgtokai](https://gitee.com/trgtokai)
2. 中国以外地区网站（未上传项目）：[https://github.com/bigcheng123](https://github.com/bigcheng123)

### 注意事项
1. 基于信息安全考虑，请勿在此平台发布 任何关于公司或你个人的信息
2. 目前组织使用的gitee/GitHub 平台 个人/组织版，非企业版，此组织发布代码仅用于个人交流学习<br>
3. 此组织发布的所有代码均来源于网络转载或个人开源，遵循中华共和国法律法规<br>
   如有侵权风险项目 或未尽法律之事宜，请立即与我们取得联系<br>
4. 联系邮箱：bigcheng123@163.com 

成立时间：2022年7月 <br>
组织管理者: Kwan <br>
成员：Li-CheY / Zhuo / Lin / Yin <br>


### 如何下载代码？
 **Step1.首先注册一个 Gitee的账号 并登陆网页** <br>
[https://gitee.com](https://gitee.com)
(公开项目不需登录Gitee即可下载，非公开项目需要登录并且加入组织）<br>

![输入图片说明](02_Image/image1.JPG)

 **Step2.打开 TRG PE 组织网址** <br>
[TRG PE 组织网址 https://gitee.com/trgtokai](https://gitee.com/trgtokai)

 **Step3.打开项目 点击右上方，下载代码** <br>

![输入图片说明](02_Image/image4.JPG)

 **Step4. 配置本地电脑的开发环境** 
1. 安装pycharm 或 其他 IDE工具
2. 安装 Anaconda python包集成工具
3. 安装依赖库 → 安装包 `pip install requirments.txt`

 **Step5. 运行代码** <br>
使用IDE （Pycharm / visualstudio）打开项目文件，调试或修改代码…… <br>

### 如何加入组织？<br>
加入组织后，你可以共享组织目录下的全部项目，并参与开发代码 <br>
加入组织的方法<br>
1. 注册账号并申请加入【TRG PE],管理员审核通过后，你的gitee账户将会被加入到 【TRG PE】<br>
2. 将你的Gitee账户邮箱，发送至管理员邮箱  bigcheng123@hotmail.com



### gitee(代码托管网站）使用指南

2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以点击 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)

### 如何使用 git工具
1. Gitee是一个 代码托管网站，与国外的 Github（开源社区）功能类似，<br>
使用方法：登陆网站查看/管理项目（存放在云端的项目）<br>
2. git 是一个软件，用于管理 控制代码的更新版本，安装在本地电脑中使用<br>
（管理存放在本地电脑的项目 → 本地修改代码后→ 上传到云端（gitee））<br>
3. 查看官方git 工具的说明资料<br>
[官方git使用指南 https://gitee.com/all-about-git](https://gitee.com/all-about-git)


#### 参考文献 或 引用代码
1.  [yolov5 https://github.com/ultralytics/yolov5/tree/v6.1](https://github.com/ultralytics/yolov5/tree/v6.1)
2.  [OpenCV](http://)
3.  [pytorch](http://)
4.  [python](http://)
5. [pyQT5](http://)
6. [输入链接说明](http://)
7. [输入链接说明](http://)
8. [输入链接说明](http://)
9. [输入链接说明](http://)<br>
10.[输入链接说明](http://)<br>


#### 特技
1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
